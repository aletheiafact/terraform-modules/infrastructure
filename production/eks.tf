module "ecr" {
  source = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//ecr"

  name = "aletheiafact-production"
}

module "eks" {
  source          = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//eks"
  name            = "production"
  vpc_id          = module.network.vpc_id
  subnets         = module.network.subnet
  min_nodes       = 1
  max_nodes       = 3
  number_of_nodes = 3
  instance_types  = ["t3.medium"]
}
