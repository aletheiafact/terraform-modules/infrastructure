module "s3" {
  source     = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//s3"
  name       = "aletheiafact-production-claimimages"
  versioning = "Enabled"

  access_control_policy = {
    user       = "msantos"
    permission = "FULL_CONTROL"
    id         = "7217dc20dd7b04ee31b917cec388860d5f35045b50e5f7dd011b4a546ee89670"
  }

  tags = {
    "Environment" = "Test"
    "ManagedBy"   = "Terraform"
  }
}
