module "network" {
  source = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//network"

  name = "aletheiafact-production"
  routes = {
    igw  = { type = "igw", cidr = "0.0.0.0/0", gateway_id = "", peer_id = "" }
    peer = { type = "peer", cidr = data.aws_vpc_peering_connection.pc.cidr_block, gateway_id = "", peer_id = module.vpc_peer.vpc_peer_id }
  }
}


module "vpc_peer" {
  source = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//vpc_peering"

  auto_accept   = true
  peer_owner_id = "625844033181"
  peer_vpc_id   = "vpc-02eada4d88f115c03"
  vpc_id        = module.network.vpc_id
  tags = {
    Name = "mongo-atlas"
  }
}
