module "ssm_environment" {
  source      = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//ssm"
  environment = var.environment
}
