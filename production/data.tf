data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_vpc_peering_connection" "pc" {
  peer_vpc_id = module.network.vpc_id
  vpc_id      = "vpc-02eada4d88f115c03"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu-*"]
  }

  owners = [data.aws_caller_identity.current.account_id]
}
