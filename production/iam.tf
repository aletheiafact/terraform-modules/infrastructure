module "instance_profile" {
  source = "/var/www/consultancy/aletheia/terraform-modules/iam/instance_profile"

  name               = "maintenance-server"
  description        = "Maintenance server instance profile"
  path               = "/"
  assume_role_policy = file("${path.module}/templates/policies/maintenance_server_role.json")

  policy_arns = [
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy",
    "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
  ]

  inline_policy = {
    name   = "EC2MaintenanceServerPolicy"
    policy = file("${path.module}/templates/policies/maintenance_server_inline-policy.json")
  }
}
