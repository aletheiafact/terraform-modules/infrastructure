module "maintenance_server" {
  source = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//ec2"

  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  instance_profile            = module.instance_profile.name
  subnet_id                   = module.network.subnet[0]
  vpc_security_group_ids      = [module.ms_sg.id]
  volume_size                 = 20
  availability_zone           = module.network.availability_zone[0]
  associate_public_ip_address = true

  user_data = templatefile("${path.module}/templates/user_data.sh", {
    host = "maintenance_server"
  })

  tags = {
    Name = "maintenance_server"
  }
}

module "ms_sg" {
  source = "git@gitlab.com:aletheiafact/terraform-modules/terraform-module-s3.git//security-groups/v2"

  name        = "maintenance_server"
  description = "Security group for maintenance server"
  vpc_id      = module.network.vpc_id

  ingress_rules = [
    {
      cidr_ipv4 = module.network.vpc_cidr
      from_port = -1
      to_port   = -1
      protocol  = "-1"
    }
  ]
}
