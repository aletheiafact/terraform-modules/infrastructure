terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.40"
    }
  }

  backend "s3" {
    bucket  = "aletheia-terraform-state"
    key     = "production/terraform.tfstate"
    region  = "us-east-1"
    profile = "aletheia"
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "aletheia"

  default_tags {
    tags = {
      ManagedBy   = "Terraform"
      Environment = "Production"
    }
  }
}
